export class Properties {

    static pref_languages: string = "languages";
    static pref_active_schemes: string = "active_schemes";
    static pref_show_flags: string = "show_flags";
    static pref_project_theme: string = "project_theme";
    static pref_show_instances_number: string = "show_instances_number";

    static setting_languages: string = "languages";
    static setting_remote_configs = "remote_configs";

    static plugin_id_rendering_engine: string = "it.uniroma2.art.semanticturkey.plugin.extpts.RenderingEngine";

}