import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ARTNode } from "../../models/ARTResources";

@Component({
	selector: "resource-list",
	templateUrl: "./resourceListComponent.html",
})
export class ResourceListComponent {
	@Input() resources: ARTNode[];
	@Output() nodeSelected = new EventEmitter<ARTNode>();

	private resourceSelected: ARTNode;

	constructor() { }

	private onResourceSelected(resource: ARTNode) {
		this.resourceSelected = resource;
		this.nodeSelected.emit(resource);
	}

}